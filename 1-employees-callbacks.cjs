/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/
const path = require('path');
const fs = require('fs');

fs.readFile(path.join(__dirname,"data.json"),"utf-8",function(err,data) {

    if(err){
        console.error("Data not read",err);
    } else {

        console.log("Data read");
        let employeeObject = JSON.parse(data);

        let employeesWithSpecificID = Object.values(employeeObject)
        .flat()
        .reduce((accumulator,currentValue) => {
            if(currentValue.id === 2 || currentValue.id === 13 || currentValue.id === 23){
                accumulator.push(currentValue);
            }
            return accumulator;
        },[]);

        fs.writeFile(path.join(__dirname,"employeesWithSpecificID.json"),JSON.stringify(employeesWithSpecificID),function(err) {

            if(err){
                console.error("File not created",err);
            } else {

                console.log("File created");
                
                let groupedEmployees = Object.values(employeeObject)
                .flat()
                .reduce((accumulator,currentValue) => {
                    if(accumulator[currentValue.company] === undefined){
                        accumulator[currentValue.company] = [currentValue];
                    } else {
                        accumulator[currentValue.company].push(currentValue);
                    }
                    return accumulator;
                },{});

                fs.writeFile(path.join(__dirname,"groupedEmployees.json"),JSON.stringify(groupedEmployees),function(err){

                    if(err){
                        console.error("File not created",err);
                    } else {

                        console.log("File created");
                        fs.readFile(path.join(__dirname,"groupedEmployees.json"),"utf-8",function(err,data){

                            if(err){
                                console.error("File not read",err);
                            } else {

                                console.log("File read");
                                let groupedDataObject = JSON.parse(data);
                                let powerpuffData = groupedDataObject["Powerpuff Brigade"];
                                fs.writeFile(path.join(__dirname,"powerPuffCompany.json"),JSON.stringify(powerpuffData),function(err){

                                    if(err){
                                        console.error("File not written",err);
                                    } else {
                                        console.log("File written");

                                        let employeeDataWithoutID2 = Object.values(employeeObject)
                                        .flat()
                                        .filter(employeeData => {
                                            return employeeData.id !== 2;
                                        });

                                        fs.writeFile(path.join(__dirname,"employeeDataWithoutID2.json"),JSON.stringify(employeeDataWithoutID2),function(err){

                                            if(err){
                                                console.error("File not written",err);
                                            } else {

                                                console.log("File written");

                                                let sortedEmployeeData = employeeDataWithoutID2
                                                .sort((employee1,employee2) => {
                                                    if(employee1.company > employee2.company){
                                                        return 1;
                                                    } else if(employee1.company < employee2.company){
                                                        return -1;
                                                    } else {
                                                        if(employee1.id > employee2.id){
                                                            return 1;
                                                        } else {
                                                            return -1;
                                                        }
                                                    }
                                                });

                                                fs.writeFile(path.join(__dirname,"sortedEmployees.json"),JSON.stringify(sortedEmployeeData),function(err){

                                                    if(err){
                                                        console.error("File not written",err);
                                                    } else {

                                                        console.log("File written");

                                                        let indexOfID92 = sortedEmployeeData
                                                        .findIndex(employeeData => {
                                                            return employeeData.id === 92;
                                                        });

                                                        let indexOfID93 = sortedEmployeeData
                                                        .findIndex(employeeData => {
                                                            return employeeData.id === 93;
                                                        });
                                            
                                                        let tempEmployeeData = sortedEmployeeData[indexOfID92];
                                                        sortedEmployeeData[indexOfID92] = sortedEmployeeData[indexOfID93];
                                                        sortedEmployeeData[indexOfID93] = tempEmployeeData;
                                                        
                                                        fs.writeFile(path.join(__dirname,"swappedEmployees.json"),JSON.stringify(sortedEmployeeData),function(err){
                                                            if(err){
                                                                console.error("File not written",err);
                                                            } else {
                                                                console.log("File written");

                                                                let evenEmployeesBirthday = sortedEmployeeData
                                                                .map((employeeData) => {
                                                                    if(employeeData.id % 2 === 0){
                                                                        
                                                                        let currentDate = new Date();
                                                                        let day = currentDate.getDate();
                                                                        let month = currentDate.getMonth() + 1;
                                                                        let year = currentDate.getFullYear();
                                                                        let birthday = `${day}/${month}/${year}`;

                                                                        employeeData["birthday"] = birthday;
                                                                    }
                                                                    return employeeData;
                                                                });

                                                                fs.writeFile(path.join(__dirname,"evenEmployeesBirthday.json"),JSON.stringify(evenEmployeesBirthday),function(err){
                                                                    if(err){
                                                                        console.error("File not written",err);
                                                                    } else {
                                                                        console.log("File written");
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                        
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});


